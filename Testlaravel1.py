import unittest
import time
import random
import string
import selenium
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
import sys



class TestLaravel(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome('D:\PETRUT\Kit Windows 10\chromedriver.exe')
        self.driver.get('https://laravel.dev-society.com/')


    def tearDown(self):
        self.driver.quit()

    def test_1_about_us(self):
        about_us = self.driver.find_element_by_xpath('//*[@id="about_page"]')
        about_us.click()
        time.sleep(1)
        self.assertIn('about', self.driver.current_url)

    def test_2_laravel(self):
        laravel = self.driver.find_element_by_xpath('//*[@id="app"]/nav/div/a')
        laravel.click()
        time.sleep(1)

    def test_3_case1_RegisterUser(self): #am creat o functie care apeleaza direct ---- vezi functia Register din def Register
        self.register('Stefanescu Petrut', 'petrutstefanescu38@gmail.com', 'cursuriazimut')
        time.sleep(2)
        self.assertIn('dashboard', self.driver.current_url)


    def test_3_case2_RegisterUser(self): #same user/email/password
        # register_button = self.driver.find_element_by_xpath('//*[@id="register_button"]')
        # register_button.click()
        #
        # name_box = self.driver.find_element_by_xpath('//*[@id="name"]')
        # name_box.clear()
        # name_box.send_keys('Stefanescu Petrut')
        #
        # email_box = self.driver.find_element_by_xpath('//*[@id="email"]')
        # email_box.clear()
        # email_box.send_keys('petrutstefanescu38@gmail.com')
        #
        # pass_box = self.driver.find_element_by_xpath('//*[@id="password"]')
        # pass_box.clear()
        # pass_box.send_keys('cursuriazimut')
        #
        # copass_box= self.driver.find_element_by_xpath('//*[@id="password-confirm"]')
        # copass_box.clear()
        # copass_box.send_keys('cursuriazimut')
        #
        # register_button = self.driver.find_element_by_xpath('//*[@id="register_account"]')
        # register_button.click()

#sau apelam direct functia Register din def Register !!!

        self.register('Stefanescu Petrut', 'petrutstefanescu38@gmail.com', 'cursuriazimut')
        time.sleep(2)

        self.assertIn('dashboard', self.driver.current_url)


    def test_3_case3_RegisterUser(self): #name ok/ email ok/ password empty
        self.register('Stefanescu Petrut', 'petrutstefanescu38@gmail.com',' ' )
        time.sleep(2)

        self.assertIn('dashboard', self.driver.current_url)

    def test_case4_RegisterUser(self):  #name empty / email ok/ password ok/ confirm ok
        self.register(' ', 'petrutstefanescu@gmail.com','cursuriazimut')
        time.sleep(2)

        self.assertIn('dashboard', self.driver.current_url)

    def test_case5_RegisterUser(self):  #name ok / email empty/ password ok/ confirm ok
        self.register('Stefanescu Petrut', ' ','cursuriazimut')
        time.sleep(2)

        self.assertIn('dashboard', self.driver.current_url)

    def test_4_login(self):
        login = self.driver.find_element_by_xpath('//*[@id="login_button"]')
        login.click()
        time.sleep(1)
        self.assertIn('login', self.driver.current_url)

        email_box = self.driver.find_element_by_xpath('//*[@id="email"]')
        email_box.clear()
        email_box.send_keys('petrutstefanescu38@gmail.com')
        time.sleep(1)

        pass_box = self.driver.find_element_by_xpath('//*[@id="password"]')
        pass_box.clear()
        pass_box.send_keys('cursuri azimut')
        time.sleep(1)

        confirmlogin = self.driver.find_element_by_xpath('//*[@id="login_user"]')
        confirmlogin.click()
        time.sleep(1)

    def login(self):
        login = self.driver.find_element_by_xpath('//*[@id="login_button"]')
        login.click()
        time.sleep(1)
        self.assertIn('login', self.driver.current_url)

        email_box = self.driver.find_element_by_xpath('//*[@id="email"]')
        email_box.clear()
        email_box.send_keys('petrutstefanescu38@gmail.com')
        time.sleep(1)

        pass_box = self.driver.find_element_by_xpath('//*[@id="password"]')
        pass_box.clear()
        pass_box.send_keys('cursuriazimut')
        time.sleep(1)

        confirmlogin = self.driver.find_element_by_xpath('//*[@id="login_user"]')
        confirmlogin.click()
        time.sleep(1)

    def register(self, name, email, password):
        register_button = self.driver.find_element_by_xpath('//*[@id="register_button"]')
        register_button.click()

        name_box = self.driver.find_element_by_xpath('//*[@id="name"]')
        name_box.clear()
        name_box.send_keys(name)

        email_box = self.driver.find_element_by_xpath('//*[@id="email"]')
        email_box.clear()
        email_box.send_keys()

        email_box = self.driver.find_element_by_xpath('//*[@id="email"]')
        email_box.clear()
        email_box.send_keys(email)
        time.sleep(1)

        pass_box = self.driver.find_element_by_xpath('//*[@id="password"]')
        pass_box.clear()
        pass_box.send_keys(password)

        copass_box = self.driver.find_element_by_xpath('//*[@id="password-confirm"]')
        copass_box.clear()
        copass_box.send_keys(password)

        register_button = self.driver.find_element_by_xpath('//*[@id="register_account"]')
        register_button.click()

    def test_6_postsbutton(self):
        self.login()

        postsbutton = self.driver.find_element_by_xpath('//*[@id="sidebar-posts"]/a')
        postsbutton.click()
        time.sleep(1)

    def test_7_addnewpost(self):
        self.login()

        postsbutton = self.driver.find_element_by_xpath('//*[@id="sidebar-posts"]/a')
        postsbutton.click()

        addnewpost = self.driver.find_element_by_xpath('//*[@id="create_post"]')
        addnewpost.click()

        self.assertIn('create', self.driver.current_url)

        title_box = self.driver.find_element_by_xpath('/html/body/main/div/div/main/form/input[2]')
        title_box.clear()
        title_box.send_keys('Test Petrut')

        write_box = self.driver.find_element_by_xpath('//*[@id="text_editor"]')
        write_box.clear()
        write_box.send_keys('Alergam la maraton')
        time.sleep(2)

        submit_button = self.driver.find_element_by_xpath('//*[@id="submit_post"]')
        submit_button.click()

        TestLaravel.id_post = self.driver.current_url.split('/')[-1]
        #variabila globala= cu ajutorul acestei functii stim care postare urmeaza sa o editam/stergem.

    def test_8_edit_post(self):
        self.login()

        edit_post = self.driver.find_element_by_xpath('//*[@id="edit-post-' + TestLaravel.id_post+ '"]')
        edit_post.click()
        #variabila globala= cu ajutorul acestei functii stim care postare urmeaza sa o editam/ stergem.

        self.assertIn('edit', self.driver.current_url)

        write_box = self.driver.find_element_by_xpath('//*[@id="text_editor"]')
        write_box.clear()
        write_box.send_keys('Alergam la maraton sambata aceasta')
        time.sleep(2)

        submit_button = self.driver.find_element_by_xpath('//*[@id="submit_post"]')
        submit_button.click()

        #driver.get('https://laravel.dev-society.com/dashboard')
        #functie refresh pagina web

    def test_9_deletepost(self):
        self.login()

        self.assertIn('dashboard', self.driver.current_url)

        delete_button = self.driver.find_element_by_xpath('//*[@id="delete-post-' + TestLaravel.id_post + '" ]')
        delete_button.click()
        #variabila globala= cu ajutorul acestei functii stim care postare urmeaza sa o editam/ stergem.
        time.sleep(2)

    def test_10_settings(self):
        self.login()

        settings = self.driver.find_element_by_xpath('//*[@id="sidebar-settings"]/a')
        settings.click()

        edit_button = self.driver.find_element_by_xpath('//*[@id="edit_account"]')
        edit_button.click()

        self.assertIn('edit', self.driver.current_url)

        change_user = self.driver.find_element_by_xpath('//*[@id="user"]')
        change_user.click()
        time.sleep(1)

        select_admin = self.driver.find_element_by_xpath('//*[@id="admin"]')
        select_admin.click()
        time.sleep(1)

        submit_button = self.driver.find_element_by_xpath('//*[@id="apply"]')
        submit_button.click()
        time.sleep(1)

    def test_11_editusers(self):
        self.login()

        users = self.driver.find_element_by_xpath('//*[@id="sidebar-users"]/a')
        users.click()

        self.assertIn('users', self.driver.current_url)

        edit_users = self.driver.find_element_by_xpath('//*[@id="edit-user-4"]')
        edit_users.click()

        name_box = self.driver.find_element_by_xpath('//*[@id="edit_name"]')
        name_box.clear()
        name_box.send_keys('Test editat Petrut')
        time.sleep(2)

        submit_button = self.driver.find_element_by_xpath('//*[@id="apply"]')
        submit_button.click()


    def test_12_deleteuser(self):
        self.login()

        settings = self.driver.find_element_by_xpath('//*[@id="sidebar-settings"]/a')
        settings.click()

        self.assertIn('settings', self.driver.current_url)

        delete_user = self.driver.find_element_by_xpath('//*[@id="delete_button"]')
        delete_user.click()


# def main():
#     test_suite = unittest.TestSuite()
# #suita de teste
#
#     test_suite.addTest(TestLaravel('test_3_case1_RegisterUser'))
#     test_suite.addTest(TestLaravel('test_11_editusers'))
#     #test_suite.addTest(TestFunctii('test_scadere'))
#
#     runner = unittest.TextTestRunner(verbosity=3)
#     runner.run(test_suite)
# #rulam suita de teste
#
# main()


def main(test_plan_doi):
    test_suite = unittest.TestSuite()

    fisier = open(test_plan_doi, 'r')
    lines = fisier.read().split('\n')
    for line in lines:
        print(line)
        test_suite.addTest(TestLaravel(line))

    fisier.close()

    runner = unittest.TextTestRunner(verbosity=3)
    runner.run(test_suite)
# #rulam suita de teste

main(sys.argv[1])